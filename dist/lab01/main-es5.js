(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-my-nav></app-my-nav>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/my-nav/my-nav.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/my-nav/my-nav.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"sidenav-container\">\n    <mat-sidenav #drawer class=\"sidenav\" fixedInViewport [attr.role]=\"(isHandset$ | async) ? 'dialog' : 'navigation'\" [mode]=\"(isHandset$ | async) ? 'over' : 'side'\" [opened]=\"(isHandset$ | async) === false\">\n        <mat-toolbar>Menu</mat-toolbar>\n        <mat-nav-list>\n            <a mat-list-item [routerLink]=\"['/list']\" routerLinkActive=\"router-link-active\">List</a>\n            <a mat-list-item [routerLink]=\"['/add']\" routerLinkActive=\"router-link-active\">Add</a>\n            <a mat-list-item [routerLink]=\"['/view']\" routerLinkActive=\"router-link-active\">View</a>\n        </mat-nav-list>\n    </mat-sidenav>\n    <mat-sidenav-content>\n        <mat-toolbar color=\"primary\">\n            <button type=\"button\" aria-label=\"Toggle sidenav\" mat-icon-button (click)=\"drawer.toggle()\" *ngIf=\"isHandset$ | async\">\n        <mat-icon aria-label=\"Side nav toggle icon\">menu</mat-icon>\n      </button>\n            <span>lab01</span>\n        </mat-toolbar>\n        <!-- Add Content Here -->\n        <router-outlet></router-outlet>\n    </mat-sidenav-content>\n</mat-sidenav-container>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/shared/file-not-found/file-not-found.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/shared/file-not-found/file-not-found.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<H2>The resource you have asked is not in the server</H2>\n<img src=\"assets/images/file-not-found.jpg\">"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/students/add/students.add.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/students/add/students.add.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar>\n  <mat-toolbar-row>\n    <span>Add new student</span>\n  </mat-toolbar-row>\n</mat-toolbar>\n<form style=\"margin-left: 1em\">\n  <mat-form-field class=\"full-width\">\n    <input matInput placeholder=\"Student Id\">\n  </mat-form-field>\n  <table class=\"full-width\" cellspacing=\"0\">\n    <tr>\n      <td>\n        <mat-form-field class=\"full-width\">\n          <input matInput placeholder=\"First name\">\n        </mat-form-field>\n      </td>\n      <td>\n        <mat-form-field class=\"full-width\">\n          <input matInput placeholder=\"surname\">\n        </mat-form-field>\n      </td>\n    </tr>\n    <div>\n      <button mat-raised-button class=\"center-button\" (click)=\"downQuantity(student)\">-</button>\n      <mat-form-field class=\"penInput\">\n        <input matInput placeholder=\"pen Amount\" class=\"center-text\" value=\"0\">\n      </mat-form-field>\n      <button mat-raised-button layout=\"row\" class=\"center-button\" (click)=\"upQuantity(student)\">+</button>\n    </div>\n  </table>\n  <mat-form-field hintLabel=\"will change to the image uploader component later\" class=\"full-width\">\n    <input matInput placeholder=\"file name\">\n  </mat-form-field>\n  <mat-form-field class=\"full-width\">\n      <textarea matInput placeholder=\"Description\"></textarea>\n    </mat-form-field>\n  <div class=\"center\">\n      <button mat-raised-button color=\"primary\" type=\"submit\" >Add</button>\n  </div>\n</form>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/students/list/students.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/students/list/students.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row justify-content-start\">\n  <div class=\"col-md-offset-3\t col-md-9\">\n    <div *ngFor=\"let student of students\">\n      <div class=\"panel panel-primary  \">\n        <div class=\"panel-heading\">\n          <h2 class=\"panel-title\">{{student.studentId }}</h2>\n        </div>\n        <div class=\"panel-body row\">\n          <div class=\"col-md-4\">\n            <p class=\"student-name\"> {{student. name | uppercase}} {{student.surname}}</p>\n            <p *ngIf=\"student.gpa > 2.5\">Good Student who get grade {{student.gpa | number:'1.2-2'}}</p>\n            <p *ngIf=\"student.gpa <= 2.5\">Bad Student who get grade {{student.gpa | number:'1.2-2'}}</p>\n          </div>\n          <div class=\"col-md-4\">\n            <img [src]=\"student.image\" [title]=\"student.name\">\n          </div>\n        </div>\n        <div class=\"row align-items-center\">\n          <button type=\"button\" class=\"btn btn-primary btn-xs col-md-1\" (click)=\"downQuantity(student)\">-</button>\n          <input type=\"text\" class=\"col-md-1\" [(ngModel)]=\"student.penAmount\">\n          <button type=\"button\" class=\"btn btn-primary btn-xs col-md-1\" (click)=\"upQuantity(student)\">+</button>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"row justify-content-end grade\">\n  <div class=\"alert alert-success col-md-offset-2 col-md-6\"> The average gpa is {{averageGpa()}} </div>\n</div>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/students/student-table/student-table.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/students/student-table/student-table.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mat-elevation-z8\">\n    \n    <mat-form-field>\n        <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n    </mat-form-field>\n    <table mat-table class=\"full-width-table\" matSort aria-label=\"Elements\">\n        <!-- Id Column -->\n        <ng-container matColumnDef=\"id\">\n            <th mat-header-cell *matHeaderCellDef>Id</th>\n            <td mat-cell *matCellDef=\"let student\">{{student.id}}</td>\n            <td mat-footer-cell *matFooterCellDef></td>\n        </ng-container>\n        <!-- StudentId Column -->\n        <ng-container matColumnDef=\"studentId\">\n            <th mat-header-cell *matHeaderCellDef>Student id</th>\n            <td mat-cell *matCellDef=\"let student\">{{student.studentId}}</td>\n            <td mat-footer-cell *matFooterCellDef></td>\n        </ng-container>\n        <!-- Name Column -->\n        <ng-container matColumnDef=\"name\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Name</th>\n            <td mat-cell *matCellDef=\"let student\">{{student.name}}</td>\n            <td mat-footer-cell *matFooterCellDef>Total </td>\n        </ng-container>\n        <!-- Surname Column -->\n        <ng-container matColumnDef=\"surname\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>Surname</th>\n            <td mat-cell *matCellDef=\"let student\">{{student.surname}}</td>\n            <td mat-footer-cell *matFooterCellDef></td>\n        </ng-container>\n        <!-- pen amount Column -->\n        <ng-container matColumnDef=\"penAmount\">\n            <th mat-header-cell *matHeaderCellDef class=\"center-text\">Pen</th>\n            <td mat-cell *matCellDef=\"let student\">\n                <div class=\"center\">\n                    <button mat-raised-button class=\"center-button\" (click)=\"downQuantity(student)\">-</button>\n                    <mat-form-field class=\"penInput\">\n                        <input matInput class=\"center-text penInput\" [(ngModel)]=\"student.penAmount\">\n                    </mat-form-field>\n                    <button mat-raised-button layout=\"row\" class=\"center-button\" (click)=\"upQuantity(student)\">+</button>\n                </div>\n            </td>\n            <td mat-footer-cell *matFooterCellDef></td>\n        </ng-container>\n        <!-- Gpa Column -->\n        <ng-container matColumnDef=\"gpa\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header>GPA</th>\n            <td mat-cell *matCellDef=\"let student\">{{student.gpa}}</td>\n            <td mat-footer-cell *matFooterCellDef> {{averageGpa() | number:'1.2-2'}} </td>\n            <td mat-footer-cell *matFooterCellDef></td>\n        </ng-container>\n        <!-- image Column -->\n        <ng-container matColumnDef=\"image\">\n            <th mat-header-cell *matHeaderCellDef class=\"center-text\">Image</th>\n            <td mat-cell *matCellDef=\"let student\" class=\"center-text\">\n                <img [src]=\"student.image\" [title]=\"student.name\" class=\"img-fluid\">\n            </td>\n            <td mat-footer-cell *matFooterCellDef></td>\n        </ng-container>\n\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n        <tr mat-footer-row *matFooterRowDef=\"displayedColumns\"></tr>\n    </table>\n\n    <mat-paginator #paginator [length]=\"dataSource?.data.length\" [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[25, 50, 100, 250]\">\n    </mat-paginator>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/students/view/students.view.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/students/view/students.view.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar>\n    <mat-toolbar-row>\n        <span>Student</span>\n    </mat-toolbar-row>\n</mat-toolbar>\n<form style=\"margin-left: 1em\">\n    <mat-form-field class=\"full-width\">\n        <input matInput placeholder=\"Student Id\" [value]=\"student?.studentId\" readonly>\n    </mat-form-field>\n    <table class=\"full-width\" cellspacing=\"0\">\n        <tr>\n            <td>\n                <mat-form-field class=\"full-width\">\n                    <input matInput placeholder=\"First name\" [value]=\"student?.name\" readonly>\n                </mat-form-field>\n            </td>\n            <td>\n                <mat-form-field class=\"full-width\">\n                    <input matInput placeholder=\"surname\" [value]=\"student?.surname\" readonly>\n                </mat-form-field>\n            </td>\n        </tr>\n        <mat-form-field class=\"penInput\">\n            <input matInput placeholder=\"pen Amount\" class=\"center-text\" [value]=\"student?.penAmount\" readonly>\n        </mat-form-field>\n    </table>\n    <div class=\"center\">\n        <mat-card class=\"img-width\">\n            <mat-card-header>\n                <mat-card-title>Image</mat-card-title>\n            </mat-card-header>\n            <img mat-card-image [attr.src]=\"student?.image\" [alt]=\"student?.name\">\n        </mat-card>\n    </div>\n    <mat-form-field class=\"full-width\">\n        <textarea matInput placeholder=\"Description\" [value]=\"student?.description\" readonly></textarea>\n    </mat-form-field>\n</form>"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_file_not_found_file_not_found_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/file-not-found/file-not-found.component */ "./src/app/shared/file-not-found/file-not-found.component.ts");




var appRoutes = [
    {
        path: '',
        redirectTo: '/list',
        pathMatch: 'full'
    },
    { path: '**', component: _shared_file_not_found_file_not_found_component__WEBPACK_IMPORTED_MODULE_3__["FileNotFoundComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(appRoutes)
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]
            ]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.name = 'SE331';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _service_student_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./service/student-service */ "./src/app/service/student-service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _service_students_file_impl_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./service/students-file-impl.service */ "./src/app/service/students-file-impl.service.ts");
/* harmony import */ var _students_list_students_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./students/list/students.component */ "./src/app/students/list/students.component.ts");
/* harmony import */ var _students_add_students_add_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./students/add/students.add.component */ "./src/app/students/add/students.add.component.ts");
/* harmony import */ var _students_view_students_view_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./students/view/students.view.component */ "./src/app/students/view/students.view.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _my_nav_my_nav_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./my-nav/my-nav.component */ "./src/app/my-nav/my-nav.component.ts");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _shared_file_not_found_file_not_found_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./shared/file-not-found/file-not-found.component */ "./src/app/shared/file-not-found/file-not-found.component.ts");
/* harmony import */ var _students_student_routing_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./students/student-routing.module */ "./src/app/students/student-routing.module.ts");
/* harmony import */ var _students_student_table_student_table_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./students/student-table/student-table.component */ "./src/app/students/student-table/student-table.component.ts");




















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _students_list_students_component__WEBPACK_IMPORTED_MODULE_8__["StudentsComponent"],
                _students_add_students_add_component__WEBPACK_IMPORTED_MODULE_9__["StudentsAddComponent"],
                _students_view_students_view_component__WEBPACK_IMPORTED_MODULE_10__["StudentsViewComponent"],
                _my_nav_my_nav_component__WEBPACK_IMPORTED_MODULE_12__["MyNavComponent"],
                _shared_file_not_found_file_not_found_component__WEBPACK_IMPORTED_MODULE_16__["FileNotFoundComponent"],
                _students_student_table_student_table_component__WEBPACK_IMPORTED_MODULE_18__["StudentTableComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"],
                _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_13__["LayoutModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_14__["MatInputModule"],
                _students_student_routing_module__WEBPACK_IMPORTED_MODULE_17__["StudentRoutingModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_15__["AppRoutingModule"]
            ],
            providers: [
                { provide: _service_student_service__WEBPACK_IMPORTED_MODULE_4__["StudentService"], useClass: _service_students_file_impl_service__WEBPACK_IMPORTED_MODULE_7__["StudentsFileImplService"] }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/my-nav/my-nav.component.css":
/*!*********************************************!*\
  !*** ./src/app/my-nav/my-nav.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidenav-container {\n  height: 100%;\n}\n\n.sidenav {\n  width: 200px;\n}\n\n.sidenav .mat-toolbar {\n  background: inherit;\n}\n\n.mat-toolbar.mat-primary {\n  position: -webkit-sticky;\n  position: sticky;\n  top: 0;\n  z-index: 1;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbXktbmF2L215LW5hdi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtBQUNkOztBQUVBO0VBQ0UsWUFBWTtBQUNkOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0Usd0JBQWdCO0VBQWhCLGdCQUFnQjtFQUNoQixNQUFNO0VBQ04sVUFBVTtBQUNaIiwiZmlsZSI6InNyYy9hcHAvbXktbmF2L215LW5hdi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNpZGVuYXYtY29udGFpbmVyIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4uc2lkZW5hdiB7XG4gIHdpZHRoOiAyMDBweDtcbn1cblxuLnNpZGVuYXYgLm1hdC10b29sYmFyIHtcbiAgYmFja2dyb3VuZDogaW5oZXJpdDtcbn1cblxuLm1hdC10b29sYmFyLm1hdC1wcmltYXJ5IHtcbiAgcG9zaXRpb246IHN0aWNreTtcbiAgdG9wOiAwO1xuICB6LWluZGV4OiAxO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/my-nav/my-nav.component.ts":
/*!********************************************!*\
  !*** ./src/app/my-nav/my-nav.component.ts ***!
  \********************************************/
/*! exports provided: MyNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyNavComponent", function() { return MyNavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var MyNavComponent = /** @class */ (function () {
    function MyNavComponent(breakpointObserver) {
        this.breakpointObserver = breakpointObserver;
        this.isHandset$ = this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["Breakpoints"].Handset)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) { return result.matches; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["shareReplay"])());
    }
    MyNavComponent.ctorParameters = function () { return [
        { type: _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["BreakpointObserver"] }
    ]; };
    MyNavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-my-nav',
            template: __webpack_require__(/*! raw-loader!./my-nav.component.html */ "./node_modules/raw-loader/index.js!./src/app/my-nav/my-nav.component.html"),
            styles: [__webpack_require__(/*! ./my-nav.component.css */ "./src/app/my-nav/my-nav.component.css")]
        })
    ], MyNavComponent);
    return MyNavComponent;
}());



/***/ }),

/***/ "./src/app/service/student-service.ts":
/*!********************************************!*\
  !*** ./src/app/service/student-service.ts ***!
  \********************************************/
/*! exports provided: StudentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentService", function() { return StudentService; });
var StudentService = /** @class */ (function () {
    function StudentService() {
    }
    return StudentService;
}());



/***/ }),

/***/ "./src/app/service/students-file-impl.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/service/students-file-impl.service.ts ***!
  \*******************************************************/
/*! exports provided: StudentsFileImplService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsFileImplService", function() { return StudentsFileImplService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _student_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./student-service */ "./src/app/service/student-service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var StudentsFileImplService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](StudentsFileImplService, _super);
    function StudentsFileImplService(http) {
        var _this = _super.call(this) || this;
        _this.http = http;
        return _this;
    }
    StudentsFileImplService.prototype.getStudent = function (id) {
        return this.http.get('assets/people.json')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (students) {
            var output = students.find(function (student) { return student.id === +id; });
            return output;
        }));
    };
    StudentsFileImplService.prototype.getStudents = function () {
        return this.http.get('assets/people.json');
    };
    StudentsFileImplService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    StudentsFileImplService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], StudentsFileImplService);
    return StudentsFileImplService;
}(_student_service__WEBPACK_IMPORTED_MODULE_3__["StudentService"]));



/***/ }),

/***/ "./src/app/shared/file-not-found/file-not-found.component.css":
/*!********************************************************************!*\
  !*** ./src/app/shared/file-not-found/file-not-found.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9maWxlLW5vdC1mb3VuZC9maWxlLW5vdC1mb3VuZC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/shared/file-not-found/file-not-found.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/shared/file-not-found/file-not-found.component.ts ***!
  \*******************************************************************/
/*! exports provided: FileNotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileNotFoundComponent", function() { return FileNotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FileNotFoundComponent = /** @class */ (function () {
    function FileNotFoundComponent() {
    }
    FileNotFoundComponent.prototype.ngOnInit = function () {
    };
    FileNotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-file-not-found',
            template: __webpack_require__(/*! raw-loader!./file-not-found.component.html */ "./node_modules/raw-loader/index.js!./src/app/shared/file-not-found/file-not-found.component.html"),
            styles: [__webpack_require__(/*! ./file-not-found.component.css */ "./src/app/shared/file-not-found/file-not-found.component.css")]
        })
    ], FileNotFoundComponent);
    return FileNotFoundComponent;
}());



/***/ }),

/***/ "./src/app/students/add/students.add.component.css":
/*!*********************************************************!*\
  !*** ./src/app/students/add/students.add.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-width {\n  width: 100%;\n}\n\n.center-button {\n  text-align: center;\n  padding-left: .5em;\n  min-width: 0.5rem;\n  max-width: 0.5rem;\n}\n\n.center-text {\n  text-align: center;\n}\n\n.center {\n  display: flex;\n  justify-content: center;\n}\n\n.penInput {\n\tmin-width:3rem;\n\tmax-width: 6rem;\n  }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudHMvYWRkL3N0dWRlbnRzLmFkZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBVztBQUNiOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0Usa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0UsYUFBYTtFQUNiLHVCQUF1QjtBQUN6Qjs7QUFDQTtDQUNDLGNBQWM7Q0FDZCxlQUFlO0VBQ2QiLCJmaWxlIjoic3JjL2FwcC9zdHVkZW50cy9hZGQvc3R1ZGVudHMuYWRkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZnVsbC13aWR0aCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uY2VudGVyLWJ1dHRvbiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAuNWVtO1xuICBtaW4td2lkdGg6IDAuNXJlbTtcbiAgbWF4LXdpZHRoOiAwLjVyZW07XG59XG5cbi5jZW50ZXItdGV4dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNlbnRlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuLnBlbklucHV0IHtcblx0bWluLXdpZHRoOjNyZW07XG5cdG1heC13aWR0aDogNnJlbTtcbiAgfVxuIl19 */"

/***/ }),

/***/ "./src/app/students/add/students.add.component.ts":
/*!********************************************************!*\
  !*** ./src/app/students/add/students.add.component.ts ***!
  \********************************************************/
/*! exports provided: StudentsAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsAddComponent", function() { return StudentsAddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var StudentsAddComponent = /** @class */ (function () {
    function StudentsAddComponent() {
    }
    StudentsAddComponent.prototype.upQuantity = function (student) {
        student.penAmount++;
    };
    StudentsAddComponent.prototype.downQuantity = function (student) {
        if (student.penAmount > 0) {
            student.penAmount--;
        }
    };
    StudentsAddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-students-add',
            template: __webpack_require__(/*! raw-loader!./students.add.component.html */ "./node_modules/raw-loader/index.js!./src/app/students/add/students.add.component.html"),
            styles: [__webpack_require__(/*! ./students.add.component.css */ "./src/app/students/add/students.add.component.css")]
        })
    ], StudentsAddComponent);
    return StudentsAddComponent;
}());



/***/ }),

/***/ "./src/app/students/list/students.component.css":
/*!******************************************************!*\
  !*** ./src/app/students/list/students.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".student-name{\n\t\tcolor: #FF0000;\n\t\tfont-size: x-large;\n\t}\n.grade {\n\t\tfont-weight: bold;\n\t}\n.featured{\n\tbackground: linear-gradient(to bottom right, red, yellow);\n}\nbutton{\n\tbox-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);\n\tmargin-top: 2px;\n}\ninput{\n\tbox-sizing: border-box;\n    border: 2px solid #ccc;\n    border-radius: 4px;\n\tmargin-left: 2px;\n\tmargin-right: 2px;\n\twidth: 60px;\n\ttext-align: center;\n\n}\n\n\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudHMvbGlzdC9zdHVkZW50cy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYztFQUNkLGtCQUFrQjtDQUNuQjtBQUNEO0VBQ0UsaUJBQWlCO0NBQ2xCO0FBQ0Q7Q0FDQyx5REFBeUQ7QUFDMUQ7QUFDQTtDQUNDLHVFQUF1RTtDQUN2RSxlQUFlO0FBQ2hCO0FBRUE7Q0FDQyxzQkFBc0I7SUFDbkIsc0JBQXNCO0lBQ3RCLGtCQUFrQjtDQUNyQixnQkFBZ0I7Q0FDaEIsaUJBQWlCO0NBQ2pCLFdBQVc7Q0FDWCxrQkFBa0I7O0FBRW5CIiwiZmlsZSI6InNyYy9hcHAvc3R1ZGVudHMvbGlzdC9zdHVkZW50cy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN0dWRlbnQtbmFtZXtcblx0XHRjb2xvcjogI0ZGMDAwMDtcblx0XHRmb250LXNpemU6IHgtbGFyZ2U7XG5cdH1cbi5ncmFkZSB7XG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XG5cdH1cbi5mZWF0dXJlZHtcblx0YmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSByaWdodCwgcmVkLCB5ZWxsb3cpO1xufVxuYnV0dG9ue1xuXHRib3gtc2hhZG93OiAwIDhweCAxNnB4IDAgcmdiYSgwLDAsMCwwLjIpLCAwIDZweCAyMHB4IDAgcmdiYSgwLDAsMCwwLjE5KTtcblx0bWFyZ2luLXRvcDogMnB4O1xufVxuXG5pbnB1dHtcblx0Ym94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjY2NjO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcblx0bWFyZ2luLWxlZnQ6IDJweDtcblx0bWFyZ2luLXJpZ2h0OiAycHg7XG5cdHdpZHRoOiA2MHB4O1xuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbn1cblxuXG4iXX0= */"

/***/ }),

/***/ "./src/app/students/list/students.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/students/list/students.component.ts ***!
  \*****************************************************/
/*! exports provided: StudentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsComponent", function() { return StudentsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_student_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../service/student-service */ "./src/app/service/student-service.ts");



var StudentsComponent = /** @class */ (function () {
    function StudentsComponent(studentService) {
        this.studentService = studentService;
    }
    StudentsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.studentService.getStudents()
            .subscribe(function (students) { return _this.students = students; });
    };
    StudentsComponent.prototype.averageGpa = function () {
        var e_1, _a;
        var sum = 0;
        if (Array.isArray(this.students)) {
            try {
                for (var _b = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](this.students), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var student = _c.value;
                    sum += student.gpa;
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
            return sum / this.students.length;
        }
        else {
            return null;
        }
    };
    StudentsComponent.prototype.upQuantity = function (student) {
        student.penAmount++;
    };
    StudentsComponent.prototype.downQuantity = function (student) {
        if (student.penAmount > 0) {
            student.penAmount--;
        }
    };
    StudentsComponent.ctorParameters = function () { return [
        { type: _service_student_service__WEBPACK_IMPORTED_MODULE_2__["StudentService"] }
    ]; };
    StudentsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-students',
            template: __webpack_require__(/*! raw-loader!./students.component.html */ "./node_modules/raw-loader/index.js!./src/app/students/list/students.component.html"),
            styles: [__webpack_require__(/*! ./students.component.css */ "./src/app/students/list/students.component.css")]
        })
    ], StudentsComponent);
    return StudentsComponent;
}());



/***/ }),

/***/ "./src/app/students/student-routing.module.ts":
/*!****************************************************!*\
  !*** ./src/app/students/student-routing.module.ts ***!
  \****************************************************/
/*! exports provided: StudentRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentRoutingModule", function() { return StudentRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _view_students_view_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./view/students.view.component */ "./src/app/students/view/students.view.component.ts");
/* harmony import */ var _add_students_add_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add/students.add.component */ "./src/app/students/add/students.add.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _student_table_student_table_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./student-table/student-table.component */ "./src/app/students/student-table/student-table.component.ts");






var StudentRoutes = [
    { path: 'add', component: _add_students_add_component__WEBPACK_IMPORTED_MODULE_3__["StudentsAddComponent"] },
    { path: 'list', component: _student_table_student_table_component__WEBPACK_IMPORTED_MODULE_5__["StudentTableComponent"] },
    { path: 'detail/:id', component: _view_students_view_component__WEBPACK_IMPORTED_MODULE_2__["StudentsViewComponent"] },
    { path: 'table', component: _student_table_student_table_component__WEBPACK_IMPORTED_MODULE_5__["StudentTableComponent"] }
];
var StudentRoutingModule = /** @class */ (function () {
    function StudentRoutingModule() {
    }
    StudentRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(StudentRoutes)
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]
            ]
        })
    ], StudentRoutingModule);
    return StudentRoutingModule;
}());



/***/ }),

/***/ "./src/app/students/student-table/student-table-datasource.ts":
/*!********************************************************************!*\
  !*** ./src/app/students/student-table/student-table-datasource.ts ***!
  \********************************************************************/
/*! exports provided: StudentTableDataSource */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentTableDataSource", function() { return StudentTableDataSource; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




// TODO: replace this with real data from your application
var EXAMPLE_DATA = [];
/**
 * Data source for the StudentTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
var StudentTableDataSource = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](StudentTableDataSource, _super);
    function StudentTableDataSource() {
        var _this = _super.call(this) || this;
        _this.data = EXAMPLE_DATA;
        return _this;
    }
    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    StudentTableDataSource.prototype.connect = function () {
        var _this = this;
        // Combine everything that affects the rendered data into one update
        // stream for the data-table to consume.
        var dataMutations = [
            Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(this.data),
            this.paginator.page,
            this.sort.sortChange,
            this.filter$.asObservable()
        ];
        // Set the paginators length
        this.paginator.length = this.data.length;
        return rxjs__WEBPACK_IMPORTED_MODULE_3__["merge"].apply(void 0, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](dataMutations)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function () {
            return _this.getFilter(_this.getPagedData(_this.getSortedData(tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](_this.data))));
        }));
    };
    /**
     *  Called when the table is being destroyed. Use this function, to clean up
     * any open connections or free any held resources that were set up during connect.
     */
    StudentTableDataSource.prototype.disconnect = function () { };
    /**
     * Paginate the data (client-side). If you're using server-side pagination,
     * this would be replaced by requesting the appropriate data from the server.
     */
    StudentTableDataSource.prototype.getPagedData = function (data) {
        var startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        return data.splice(startIndex, this.paginator.pageSize);
    };
    /**
     * Sort the data (client-side). If you're using server-side sorting,
     * this would be replaced by requesting the appropriate data from the server.
     */
    StudentTableDataSource.prototype.getSortedData = function (data) {
        var _this = this;
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }
        return data.sort(function (a, b) {
            var isAsc = _this.sort.direction === 'asc';
            switch (_this.sort.active) {
                case 'name': return compare(a.name, b.name, isAsc);
                case 'id': return compare(+a.id, +b.id, isAsc);
                case 'surname': return compare(a.surname, b.surname, isAsc);
                default: return 0;
            }
        });
    };
    // load data from the user
    StudentTableDataSource.prototype.getFilter = function (data) {
        var filter = this.filter$.getValue();
        if (filter === '') {
            return data;
        }
        return data.filter(function (student) {
            return (student.name.toLowerCase().includes(filter) || student.surname.toLowerCase().includes(filter));
        });
    };
    return StudentTableDataSource;
}(_angular_cdk_collections__WEBPACK_IMPORTED_MODULE_1__["DataSource"]));

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}


/***/ }),

/***/ "./src/app/students/student-table/student-table.component.css":
/*!********************************************************************!*\
  !*** ./src/app/students/student-table/student-table.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* Structure */\n\ntable {\n    width: 100%;\n}\n\n.mat-form-field {\n    font-size: 14px;\n    width: 100%;\n}\n\nimg {\n    max-width: 20rem;\n    max-height: 20rem;\n}\n\ntr.mat-footer-row {\n    font-weight: bold;\n}\n\n.penInput {\n    min-width: 2rem;\n    max-width: 2rem;\n}\n\n.mat-cell {\n    padding: 8px 8px 8px 0;\n}\n\n.mat-raised-button {\n    min-width: 0.5rem;\n    max-width: 0.5rem;\n}\n\n.center-button {\n    text-align: center;\n    padding-left: .5em;\n}\n\n.center-text {\n    text-align: center;\n}\n\n.center {\n    display: flex;\n    justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudHMvc3R1ZGVudC10YWJsZS9zdHVkZW50LXRhYmxlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsY0FBYzs7QUFFZDtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGVBQWU7SUFDZixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksZUFBZTtJQUNmLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSxzQkFBc0I7QUFDMUI7O0FBRUE7SUFDSSxpQkFBaUI7SUFDakIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGFBQWE7SUFDYix1QkFBdUI7QUFDM0IiLCJmaWxlIjoic3JjL2FwcC9zdHVkZW50cy9zdHVkZW50LXRhYmxlL3N0dWRlbnQtdGFibGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIFN0cnVjdHVyZSAqL1xuXG50YWJsZSB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5tYXQtZm9ybS1maWVsZCB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG5pbWcge1xuICAgIG1heC13aWR0aDogMjByZW07XG4gICAgbWF4LWhlaWdodDogMjByZW07XG59XG5cbnRyLm1hdC1mb290ZXItcm93IHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLnBlbklucHV0IHtcbiAgICBtaW4td2lkdGg6IDJyZW07XG4gICAgbWF4LXdpZHRoOiAycmVtO1xufVxuXG4ubWF0LWNlbGwge1xuICAgIHBhZGRpbmc6IDhweCA4cHggOHB4IDA7XG59XG5cbi5tYXQtcmFpc2VkLWJ1dHRvbiB7XG4gICAgbWluLXdpZHRoOiAwLjVyZW07XG4gICAgbWF4LXdpZHRoOiAwLjVyZW07XG59XG5cbi5jZW50ZXItYnV0dG9uIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZy1sZWZ0OiAuNWVtO1xufVxuXG4uY2VudGVyLXRleHQge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNlbnRlciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/students/student-table/student-table.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/students/student-table/student-table.component.ts ***!
  \*******************************************************************/
/*! exports provided: StudentTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentTableComponent", function() { return StudentTableComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm5/sort.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _student_table_datasource__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./student-table-datasource */ "./src/app/students/student-table/student-table-datasource.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var src_app_service_student_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/service/student-service */ "./src/app/service/student-service.ts");








var StudentTableComponent = /** @class */ (function () {
    function StudentTableComponent(studentService) {
        this.studentService = studentService;
        /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
        this.displayedColumns = ['id', 'studentId', 'name', 'surname', 'image', 'penAmount', 'gpa'];
    }
    StudentTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.studentService.getStudents()
            .subscribe(function (students) {
            _this.dataSource = new _student_table_datasource__WEBPACK_IMPORTED_MODULE_5__["StudentTableDataSource"]();
            _this.dataSource.data = students;
            _this.dataSource.sort = _this.sort;
            _this.dataSource.paginator = _this.paginator;
            _this.table.dataSource = _this.dataSource;
            _this.students = students;
            _this.filter$ = new rxjs__WEBPACK_IMPORTED_MODULE_6__["BehaviorSubject"]('');
            _this.dataSource.filter$ = _this.filter$;
        });
    };
    StudentTableComponent.prototype.ngAfterViewInit = function () {
    };
    StudentTableComponent.prototype.applyFilter = function (filterValue) {
        this.filter$.next(filterValue.trim().toLowerCase());
    };
    StudentTableComponent.prototype.averageGpa = function () {
        var e_1, _a;
        var sum = 0;
        if (Array.isArray(this.students)) {
            try {
                for (var _b = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](this.students), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var student = _c.value;
                    sum += student.gpa;
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
            return sum / this.students.length;
        }
        else {
            return null;
        }
    };
    StudentTableComponent.prototype.upQuantity = function (student) {
        student.penAmount++;
    };
    StudentTableComponent.prototype.downQuantity = function (student) {
        if (student.penAmount > 0) {
            student.penAmount--;
        }
    };
    StudentTableComponent.ctorParameters = function () { return [
        { type: src_app_service_student_service__WEBPACK_IMPORTED_MODULE_7__["StudentService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_paginator__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"], { static: false })
    ], StudentTableComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_sort__WEBPACK_IMPORTED_MODULE_3__["MatSort"], { static: false })
    ], StudentTableComponent.prototype, "sort", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material_table__WEBPACK_IMPORTED_MODULE_4__["MatTable"], { static: false })
    ], StudentTableComponent.prototype, "table", void 0);
    StudentTableComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-student-table',
            template: __webpack_require__(/*! raw-loader!./student-table.component.html */ "./node_modules/raw-loader/index.js!./src/app/students/student-table/student-table.component.html"),
            styles: [__webpack_require__(/*! ./student-table.component.css */ "./src/app/students/student-table/student-table.component.css")]
        })
    ], StudentTableComponent);
    return StudentTableComponent;
}());



/***/ }),

/***/ "./src/app/students/view/students.view.component.css":
/*!***********************************************************!*\
  !*** ./src/app/students/view/students.view.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-width {\n\twidth: 100%;\n  }\n  \n  .center-button {\n\ttext-align: center;\n\tpadding-left: .5em;\n\tmin-width: 0.5rem;\n\tmax-width: 0.5rem;\n  }\n  \n  .center-text {\n\ttext-align: center;\n  }\n  \n  .center {\n\tdisplay: flex;\n\tjustify-content: center;\n  }\n  \n  .penInput {\n\t  min-width:3rem;\n\t  max-width: 6rem;\n\t}\n  \n  .img-width{\n\t\tmax-width: 400px;\n\t\t\n\t}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3R1ZGVudHMvdmlldy9zdHVkZW50cy52aWV3LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Q0FDQyxXQUFXO0VBQ1Y7O0VBRUE7Q0FDRCxrQkFBa0I7Q0FDbEIsa0JBQWtCO0NBQ2xCLGlCQUFpQjtDQUNqQixpQkFBaUI7RUFDaEI7O0VBRUE7Q0FDRCxrQkFBa0I7RUFDakI7O0VBRUE7Q0FDRCxhQUFhO0NBQ2IsdUJBQXVCO0VBQ3RCOztFQUNBO0dBQ0MsY0FBYztHQUNkLGVBQWU7Q0FDakI7O0VBRUE7RUFDQyxnQkFBZ0I7O0NBRWpCIiwiZmlsZSI6InNyYy9hcHAvc3R1ZGVudHMvdmlldy9zdHVkZW50cy52aWV3LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZnVsbC13aWR0aCB7XG5cdHdpZHRoOiAxMDAlO1xuICB9XG4gIFxuICAuY2VudGVyLWJ1dHRvbiB7XG5cdHRleHQtYWxpZ246IGNlbnRlcjtcblx0cGFkZGluZy1sZWZ0OiAuNWVtO1xuXHRtaW4td2lkdGg6IDAuNXJlbTtcblx0bWF4LXdpZHRoOiAwLjVyZW07XG4gIH1cbiAgXG4gIC5jZW50ZXItdGV4dCB7XG5cdHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICBcbiAgLmNlbnRlciB7XG5cdGRpc3BsYXk6IGZsZXg7XG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB9XG4gIC5wZW5JbnB1dCB7XG5cdCAgbWluLXdpZHRoOjNyZW07XG5cdCAgbWF4LXdpZHRoOiA2cmVtO1xuXHR9XG5cblx0LmltZy13aWR0aHtcblx0XHRtYXgtd2lkdGg6IDQwMHB4O1xuXHRcdFxuXHR9Il19 */"

/***/ }),

/***/ "./src/app/students/view/students.view.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/students/view/students.view.component.ts ***!
  \**********************************************************/
/*! exports provided: StudentsViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudentsViewComponent", function() { return StudentsViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_service_student_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/student-service */ "./src/app/service/student-service.ts");




var StudentsViewComponent = /** @class */ (function () {
    function StudentsViewComponent(route, studentService) {
        this.route = route;
        this.studentService = studentService;
    }
    StudentsViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params
            .subscribe(function (params) {
            _this.studentService.getStudent(+params['id'])
                .subscribe(function (inputStudent) { return _this.student = inputStudent; });
        });
    };
    StudentsViewComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: src_app_service_student_service__WEBPACK_IMPORTED_MODULE_3__["StudentService"] }
    ]; };
    StudentsViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-students-view',
            template: __webpack_require__(/*! raw-loader!./students.view.component.html */ "./node_modules/raw-loader/index.js!./src/app/students/view/students.view.component.html"),
            styles: [__webpack_require__(/*! ./students.view.component.css */ "./src/app/students/view/students.view.component.css")]
        })
    ], StudentsViewComponent);
    return StudentsViewComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/apple/lab/lab05/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map